/*=============================================================================
 * Copyright (c) 2020, David Broin <davidmbroin@gmail.com>
 * All rights reserved.
 * License: mit (see LICENSE.txt)
 * Date: 2020/06/09
 *===========================================================================*/

/*=====[Avoid multiple inclusion - begin]====================================*/

#ifndef _CLI_H_
#define _CLI_H_

/*=====[Inclusions of public function dependencies]==========================*/

#include "FreeRTOS.h"
#include "queue.h"

/*=====[C++ - begin]=========================================================*/

#ifdef __cplusplus
extern "C" {
#endif

/*=====[Definition macros of public constants]===============================*/

/*=====[Public function-like macros]=========================================*/

#ifndef elementsof
#define elementsof(x)  (sizeof(x) / sizeof((x)[0]))
#endif

/*=====[Definitions of public data types]====================================*/

/*=====[Prototypes (declarations) of public functions]=======================*/

void cli_init(xQueueHandle *long_poll_queue,
xQueueHandle *long_poll_ans_queue);

void cli_task();

/*=====[Prototypes (declarations) of public interrupt functions]=============*/

void on_cli_rx(void *noUsado);

void on_cli_tx(void *noUsado);

/*=====[C++ - end]===========================================================*/

#ifdef __cplusplus
}
#endif

/*=====[Avoid multiple inclusion - end]======================================*/

#endif /* _CLI_H_ */

