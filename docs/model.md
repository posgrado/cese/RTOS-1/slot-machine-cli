[[_TOC_]]

## Contexto de Sistema
```plantuml
!includeurl https://raw.githubusercontent.com/RicardoNiepel/C4-PlantUML/master/C4_Context.puml
' uncomment the following line and comment the first to use locally
' !include C4_Context.puml

title Interfaz de linea de comandos para protocolo SAS

Person(user, "Usuario", "Persona que quiere conocer el estado de la máquina tragamonedas.")
System(edu_ciaa, "EDU CIAA NXP", "Permite obtener información de contabilidad con comandos simples.")
Enterprise_Boundary(c0, "Slot Machine") {
    System_Ext(sas_interface, "SAS Interface", "Entrega información de la máquina usando protocolo SAS.")
}

Rel_Neighbor(user, edu_ciaa, "UART CLI", "Commands")
Rel_Back_Neighbor(user, edu_ciaa, "UART CLI", "Echo and response")
Rel_Neighbor(edu_ciaa, sas_interface, "SAS Protocol", "Requests")
Rel_Back_Neighbor(edu_ciaa, sas_interface, "SAS Protocol", "Answers")
```
## Comunicación entre sistemas
```mermaid
sequenceDiagram
    participant Usuario
    participant CIAA
    participant Slot
    CIAA->>Usuario: sas_cli>
    loop Espera comandos
        CIAA->>CIAA: Caracter en UART?
        CIAA->>+Slot: Algun evento nuevo?
        Slot-->>-CIAA: xx
    end
    Usuario->>+CIAA: ?
    CIAA-->>-Usuario: [lista de comandos]
    CIAA->>Usuario: sas_cli>
    loop Espera comandos
        CIAA->>CIAA: Caracter en UART?
        CIAA->>+Slot: Algun evento nuevo?
        Slot-->>-CIAA: xx
    end
    Usuario->>+CIAA: eventlist
    CIAA-->>-Usuario: [lista de eventos]
    CIAA->>Usuario: sas_cli>
    loop Espera comandos
        CIAA->>CIAA: Caracter en UART?
        CIAA->>+Slot: Algun evento nuevo?
        Slot-->>-CIAA: xx
    end
    Usuario->>+CIAA: currentcredit
    CIAA->>+Slot: 011A
    Slot-->>-CIAA: 011AxxxxCRC
    CIAA-->>-Usuario: $X.XX
    CIAA->>Usuario: sas_cli>
    loop Espera comandos
        CIAA->>CIAA: Caracter en UART?
        CIAA->>+Slot: Algun evento nuevo?
        Slot-->>-CIAA: xx
    end
    Usuario->>+CIAA: meters
    CIAA->>+Slot: 011C
    Slot-->>-CIAA: 011Cxxxxxxxxxx...CRC
    CIAA-->>-Usuario: [Contadores]
    CIAA->>Usuario: sas_cli>
    loop Espera comandos
        CIAA->>CIAA: Caracter en UART?
        CIAA->>+Slot: Algun evento nuevo?
        Slot-->>-CIAA: xx
    end
```

## Creacion de tareas
```mermaid
graph TB
    id0(Inicializar)-->id1
    id1(Crear cli_task) -- pdPASS --> id2
    id2(Crear generalPoll_task) -- pdPASS --> id3
    id3(Crear longPoll_task) -- pdPASS --> id4
    id4(Crear button_task) -- pdPASS --> id5(Iniciar Scheduler)
    id1(Crear cli_task) -- pdFAIL --> id6
    id2(Crear generalPoll_task) -- pdFAIL --> id6
    id3(Crear longPoll_task) -- pdFAIL --> id6
    id4(Crear button_task) -- pdFAIL --> id6(LED estado de error)
```
