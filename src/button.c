/*=====button===========================================================*/

/*=============================================================================
 * Copyright (c) 2020, David Broin <davidmbroin@gmail.com>
 * All rights reserved.
 * License: mit (see LICENSE.txt)
 * Date: 2020/06/10
 *===========================================================================*/

/*=====[Inclusion of own header]=============================================*/

#include "button.h"
#include "gpio_irq.h"

#include "sapi.h"
#include "FreeRTOS.h"
#include "semphr.h"
#include "task.h"
#include "sapi_gpio.h"

/*=====[Inclusions of private function dependencies]=========================*/

/*=====[Definition macros of private constants]==============================*/

/*=====[Private function-like macros]========================================*/

/*=====[Definitions of private data types]===================================*/

/*=====[Definitions of external public global variables]=====================*/

/*=====[Definitions of public global variables]==============================*/

/*=====[Definitions of private global variables]=============================*/

static SemaphoreHandle_t tec1_pressed_semaphore;

static SemaphoreHandle_t tec2_pressed_semaphore;

static SemaphoreHandle_t poll_rate_mutex;

static uint8_t *pollRate = NULL;

/*=====[Prototypes (declarations) of external public functions]==============*/

/*=====[Prototypes (declarations) of private functions]======================*/

/*=====[Implementations of public functions]=================================*/

void button_init(uint8_t * poll_rate_ptr) {
	pollRate = poll_rate_ptr;
	// Seteo un callback al evento de flanco descendente y habilito su interrupcion
	gpioCallbackSet(TEC1, INT_FALL, on_tec1_press, NULL);
	// Seteo un callback al evento de flanco descendente y habilito su interrupcion
	gpioCallbackSet(TEC2, INT_FALL, on_tec2_press, NULL);

	tec1_pressed_semaphore = xSemaphoreCreateBinary();
	tec2_pressed_semaphore = xSemaphoreCreateBinary();
	poll_rate_mutex = xSemaphoreCreateMutex();

	// Habilito todas las interrupciones de TEC1 y TEC2
	gpioInterrupt(TEC1, TRUE);
	gpioInterrupt(TEC2, TRUE);
}

uint8_t button_get_poll_rate() {
	return *pollRate;
}

void tec1_task() {
	for (;;) {
		if (pdPASS == xSemaphoreTake(tec1_pressed_semaphore, portMAX_DELAY)) {
			vTaskDelay(60 / portTICK_RATE_MS);
			if (gpioRead(TEC1) == 0 && *pollRate > 40) {
				xSemaphoreTake(poll_rate_mutex, portMAX_DELAY);
				*pollRate -= 10;
				xSemaphoreGive(poll_rate_mutex);
			}
		} else {
			gpioWrite(LEDB, ON);
			while (TRUE)
				;
		}
		//Habilito la interrupcion nuevamente
		gpioInterrupt(TEC1, TRUE);
	}
}

void tec2_task() {
	for (;;) {
		if (pdPASS == xSemaphoreTake(tec2_pressed_semaphore, portMAX_DELAY)) {
			vTaskDelay(60 / portTICK_RATE_MS);
			if (gpioRead(TEC2) == 0 && *pollRate < 200) {
				xSemaphoreTake(poll_rate_mutex, portMAX_DELAY);
				*pollRate += 10;
				xSemaphoreGive(poll_rate_mutex);
			}
		} else {
			gpioWrite(LEDB, ON);
			while (TRUE)
				;
		}
		//Habilito la interrupcion nuevamente
		gpioInterrupt(TEC2, TRUE);
	}
}

/*=====[Implementations of interrupt functions]==============================*/

void on_tec1_press(void *noUsado) {
	//Deshabilito las interrupciones mientras proceso la tecla presionada
	gpioInterrupt(TEC1, FALSE);
	BaseType_t xHigherPriorityTaskWoken = pdFALSE;

	xSemaphoreGiveFromISR(tec1_pressed_semaphore, &xHigherPriorityTaskWoken);

	portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}

void on_tec2_press(void *noUsado) {
	//Deshabilito las interrupciones mientras proceso la tecla presionada
	gpioInterrupt(TEC2, FALSE);
	BaseType_t xHigherPriorityTaskWoken = pdFALSE;

	xSemaphoreGiveFromISR(tec2_pressed_semaphore, &xHigherPriorityTaskWoken);

	portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}

/*=====[Implementations of private functions]================================*/
