/*=============================================================================
 * Copyright (c) 2020, David Broin <davidmbroin@gmail.com>
 * All rights reserved.
 * License: mit (see LICENSE.txt)
 * Date: 2020/06/05
 *===========================================================================*/

/*=====[Inclusions of function dependencies]=================================*/

#include "slot_machine_cli.h"
#include "error_handle.h"
#include "button.h"
#include "slot_machine.h"
#include "cli.h"

#include "FreeRTOS.h"
#include "FreeRTOSConfig.h"
#include "task.h"

#include "sapi.h"

/*=====[Definition macros of private constants]==============================*/

/*=====[Definitions of extern global variables]==============================*/

/*=====[Definitions of public global variables]==============================*/

/*=====[Definitions of private global variables]=============================*/

static uint8_t pollRate = 40;

static xQueueHandle long_poll_req_queue;

static xQueueHandle long_poll_ans_queue;

/*=====[Main function, program entry point after power on or reset]==========*/

int main(void) {
	BaseType_t task_result;

	boardInit();
	button_init(&pollRate);
	long_poll_req_queue = xQueueCreate(5, sizeof(uint8_t));
	if (pdFAIL != long_poll_req_queue) {
		long_poll_ans_queue = xQueueCreate(50, sizeof(uint8_t));
	} else {
		error();
	}
	if (pdFAIL != long_poll_ans_queue) {
		init_slot_task(&long_poll_req_queue, &long_poll_ans_queue);
		cli_init(&long_poll_req_queue, &long_poll_ans_queue);
	} else {
		error();
	}

	if (pdFAIL != long_poll_req_queue) {
		// Create a task in freeRTOS with dynamic memory
		task_result = xTaskCreate(cli_task, (const char *) "cli task",
		configMINIMAL_STACK_SIZE * 2, 0,
		tskIDLE_PRIORITY + 1, 0);
	} else {
		error();
	}

	if (pdPASS == task_result) {
		// Create a task in freeRTOS with dynamic memory
		task_result = xTaskCreate(tec1_task,
				(const char *) "Decrease Poll Rate Task",
				configMINIMAL_STACK_SIZE * 2, 0,
				tskIDLE_PRIORITY + 2, 0);
	} else {
		error();
	}
	if (pdPASS == task_result) {
		// Create a task in freeRTOS with dynamic memory
		task_result = xTaskCreate(tec2_task,
				(const char *) "Increase Poll Rate Task",
				configMINIMAL_STACK_SIZE * 2, 0,
				tskIDLE_PRIORITY + 1, 0);
	} else {
		error();
	}

	if (pdPASS == task_result) {
		// Create a task in freeRTOS with dynamic memory
		task_result = xTaskCreate(slot_task, (const char *) "Slot Machine Task",
		configMINIMAL_STACK_SIZE * 2, &pollRate,
		tskIDLE_PRIORITY + 1, 0);
	} else {
		error();
	}

	if (pdPASS == task_result) {
		vTaskStartScheduler(); // Initialize scheduler
	} else {
		error();
	}

	while ( true)
		; // If reach heare it means that the scheduler could not start

	// YOU NEVER REACH HERE, because this program runs directly or on a
	// microcontroller and is not called by any Operating System, as in the
	// case of a PC program.
	return 0;
}
