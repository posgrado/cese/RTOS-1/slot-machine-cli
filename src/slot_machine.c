/*=====generalPoll===========================================================*/

/*=============================================================================
 * Copyright (c) 2020, David Broin <davidmbroin@gmail.com>
 * All rights reserved.
 * License: mit (see LICENSE.txt)
 * Date: 2020/06/10
 *===========================================================================*/

/*=====[Inclusion of own header]=============================================*/

#include "slot_machine.h"
#include "error_handle.h"
#include "FreeRTOS.h"
#include "task.h"
#include "sapi.h"
#include "queue.h"

/*=====[Inclusions of private function dependencies]=========================*/

/*=====[Definition macros of private constants]==============================*/

/*=====[Private function-like macros]========================================*/

/*=====[Definitions of private data types]===================================*/

typedef enum {
	INIT, CONNECT, GENERAL_POLL_REQ, LONG_POLL_REQ, LONG_POLL_ANS
} fsmState_t;

/*=====[Definitions of external public global variables]=====================*/

/*=====[Definitions of public global variables]==============================*/

/*=====[Definitions of private global variables]=============================*/

static xQueueHandle slot_read_queue;

static xQueueHandle slot_write_queue;

static xQueueHandle *long_poll_req_queue_ptr;

static xQueueHandle *long_poll_ans_queue_ptr;

/*=====[Prototypes (declarations) of external public functions]==============*/

/*=====[Prototypes (declarations) of private functions]======================*/

static void slot_tx_byte(uint8_t c);
static void slot_tx_byte_array(const uint8_t* byte_array, uint32_t byteArrayLen);

/*=====[Implementations of public functions]=================================*/

void init_slot_task(xQueueHandle *long_poll_queue,
xQueueHandle *long_poll_ans_queue) {

	/* Inicializar la UART_USB junto con las interrupciones de Tx y Rx */
	uartConfig(UART_232, 19200);
	// Seteo un callback al evento de recepcion y habilito su interrupcion
	uartCallbackSet(UART_232, UART_RECEIVE, on_slot_rx, NULL);
	// Seteo un callback al evento de recepcion y habilito su interrupcion
	uartCallbackSet(UART_232, UART_TRANSMITER_FREE, on_slot_tx, NULL);
	// Habilito todas las interrupciones de UART_USB
	uartInterrupt(UART_232, true);
	slot_write_queue = xQueueCreate(50, sizeof(uint8_t));
	if (pdFAIL != slot_write_queue) {
		slot_read_queue = xQueueCreate(50, sizeof(uint8_t));
	} else {
		error();
	}
	if (pdFAIL != slot_read_queue) {
		long_poll_req_queue_ptr = long_poll_queue;
		long_poll_ans_queue_ptr = long_poll_ans_queue;
	} else {
		error();
	}
}

void slot_task(void * pvParameters) {
	fsmState_t sasfsmState;
	uint8_t sas_address;
	uint8_t command_code;
	uint8_t command_length;
	uint8_t ByteReceived;
	uint8_t message[100];
	uint8_t length = 0;

	sasfsmState = INIT;   // Set initial state

	uint8_t *poll_rate_ptr = (uint8_t*) pvParameters;
	portTickType xPeriodicity = *poll_rate_ptr / portTICK_RATE_MS;
	portTickType xLastWakeTime = xTaskGetTickCount();

	// ----- Task repeat for ever -------------------------
	while (TRUE) {
		gpioToggle(LED3);
		switch (sasfsmState) {
		case INIT:
			//Espera por chirping
			xQueueReceive(slot_read_queue, &ByteReceived, portMAX_DELAY);
			sas_address = ByteReceived;
			//Espera una vez más para validar la sas address
			xQueueReceive(slot_read_queue, &ByteReceived, portMAX_DELAY);
			if (sas_address == ByteReceived) {
				sasfsmState = CONNECT;
			}
			break;
		case CONNECT:
			message[0] = 0x80;
			message[1] = 0x80 | sas_address;
			slot_tx_byte_array(message, 2);
			if (pdTRUE == xQueueReceive(slot_read_queue, &ByteReceived,
			portMAX_DELAY)) {
				sasfsmState = GENERAL_POLL_REQ;
				vTaskDelayUntil(&xLastWakeTime,
						*poll_rate_ptr / portTICK_RATE_MS);
			} else {
				sasfsmState = INIT;
			}
			break;
		case GENERAL_POLL_REQ:
			if (uxQueueMessagesWaiting(*long_poll_req_queue_ptr)) {
				sasfsmState = LONG_POLL_REQ;
				break;
			}
			slot_tx_byte(0x80 | sas_address);
			xQueueReceive(slot_read_queue, &ByteReceived, portMAX_DELAY);
			vTaskDelayUntil(&xLastWakeTime, *poll_rate_ptr / portTICK_RATE_MS);
			slot_tx_byte(0x80);
			break;
		case LONG_POLL_REQ:
			xQueueReceive(*long_poll_req_queue_ptr, &command_code,
			portMAX_DELAY);
			message[0] = sas_address;
			message[1] = command_code;
			slot_tx_byte_array(message, 2);
			switch (command_code) {
			case 0x1A:
				command_length = 8;
				break;
			case 0x1C:
				command_length = 36;
				break;
			case 0x1F:
				command_length = 24;
				break;
			}
			length = 0;
			sasfsmState = LONG_POLL_ANS;
			break;
		case LONG_POLL_ANS:
			xQueueReceive(slot_read_queue, &ByteReceived, portMAX_DELAY);
			xQueueSend(*long_poll_ans_queue_ptr, &ByteReceived, 0);
			message[length] = ByteReceived;
			length++;
			if (command_length == length) {
				sasfsmState = GENERAL_POLL_REQ;
			}
			break;
		default:
			sasfsmState = INIT;
			break;
		}
	}
}
/*=====[Implementations of interrupt functions]==============================*/

void on_slot_rx(void *noUsado) {
	BaseType_t xHigherPriorityTaskWoken = pdFALSE;

	uint8_t c = uartRxRead(UART_232);

	if (errQUEUE_FULL
			!= xQueueSendFromISR(slot_read_queue, &c, &xHigherPriorityTaskWoken)) {
		portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
	} else {
		error();
	}
}

void on_slot_tx(void *noUsado) {
	BaseType_t xHigherPriorityTaskWoken = pdFALSE;
	uint8_t c;

	if (xQueueReceiveFromISR(slot_write_queue, &c, &xHigherPriorityTaskWoken)) {
		uartTxWrite(UART_232, c);
	} else {
		uartCallbackClr(UART_232, UART_TRANSMITER_FREE);
	}
	portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}

/*=====[Implementations of private functions]================================*/

static void slot_tx_byte(uint8_t c) {
	slot_tx_byte_array(&c, 1);
}

static void slot_tx_byte_array(const uint8_t* byte_array, uint32_t byteArrayLen) {
	uint8_t c = '\0';
	uint8_t i = 0;
	do {
		c = byte_array[i++];
		if (errQUEUE_FULL == xQueueSend(slot_write_queue, &c, 0)) {
			error();
		}
	} while (c != '\0' && i < byteArrayLen);
	uartCallbackSet(UART_232, UART_TRANSMITER_FREE, on_slot_tx, NULL);
	uartSetPendingInterrupt(UART_232);
}
